process.env.VUE_APP_VERSION = require('./package.json').version
var path = require('path')
module.exports = {
  configureWebpack: {
    optimization: {
      splitChunks: {
        chunks: 'all'
      }
    }
  },
  devServer: {
    port: 3000,
    disableHostCheck: true,
    proxy: {
      '^/api': {
        target: 'http://35.240.241.4'
      }
    }
  },
  publicPath: '/',
  pluginOptions: {
    'style-resources-loader': {
      preProcessor: 'scss',
      patterns: [path.resolve(__dirname, './src/styles/scss/main.scss')]
    }
  }
}
