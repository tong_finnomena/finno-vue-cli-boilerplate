module.exports = {
  root: true,
  env: {
    node: true
  },
  extends: ['plugin:vue/recommended', '@vue/prettier'],
  rules: {
    'no-console': [
      'warn',
      {
        allow: ['warn', 'error']
      }
    ],
    'max-lines': [
      'warn',
      {
        max: 300,
        skipBlankLines: true,
        skipComments: true
      }
    ],
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    semi: ['error', 'never'],
    'comma-dangle': ['error', 'never'],
    'import/no-unresolved': 'off',
    'import/prefer-default-export': 'off',
    'no-shadow': ['error', { allow: ['state'] }],
    'no-param-reassign': ['error', { props: false }],
    'linebreak-style': 0,
    'prettier/prettier': [
      'warn',
      {
        singleQuote: true,
        semi: false,
        trailingComma: 'none',
        printWidth: 120
      }
    ],
    'arrow-parens': 0
  },
  parserOptions: {
    parser: 'babel-eslint'
  }
}
