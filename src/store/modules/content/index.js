import { PENDING } from '@/store/status'
import { getters } from './getters'
import { actions } from './actions'
import { mutations } from './mutations'

const state = () => ({
  list: {
    response: {},
    status: PENDING,
    error: null
  },
  detail: {
    response: {},
    status: PENDING,
    error: null
  }
})

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
