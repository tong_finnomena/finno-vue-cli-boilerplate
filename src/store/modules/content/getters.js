export const getters = {
  getContentList(state) {
    return state.list.response.data
  },
  getContentDetail(state) {
    return state.detail.response.data
  }
}
