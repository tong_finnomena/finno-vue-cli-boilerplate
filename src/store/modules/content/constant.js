export const eventsFetchContentList = {
  PENDING: 'thaipbs/list/FETCH',
  FULFILLED: 'thaipbs/list/FETCH_SUCCEEDED',
  REJECTED: 'thaipbs/list/FETCH_FAILED'
}

export const eventsFetchContentDetail = {
  PENDING: 'thaipbs/detail/FETCH',
  FULFILLED: 'thaipbs/detail/FETCH_SUCCEEDED',
  REJECTED: 'thaipbs/detail/FETCH_FAILED'
}
