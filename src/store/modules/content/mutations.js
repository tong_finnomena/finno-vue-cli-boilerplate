import { eventsFetchContentList, eventsFetchContentDetail } from './constant'
import { PENDING, FULFILLED, REJECTED } from '@/store/status'

export const mutations = {
  [eventsFetchContentList.PENDING](state) {
    state.list.status = PENDING
    state.list.error = null
  },
  [eventsFetchContentList.FULFILLED](state, payload) {
    state.list.response = payload
    state.list.status = FULFILLED
    state.list.error = null
  },
  [eventsFetchContentList.REJECTED](state, payload) {
    state.list.status = REJECTED
    state.list.error = payload
    state.list.response = {}
  },
  [eventsFetchContentDetail.PENDING](state) {
    state.detail.status = PENDING
    state.detail.error = null
  },
  [eventsFetchContentDetail.FULFILLED](state, payload) {
    state.detail.response = payload
    state.detail.status = FULFILLED
    state.detail.error = null
  },
  [eventsFetchContentDetail.REJECTED](state, payload) {
    state.detail.status = REJECTED
    state.detail.error = payload
    state.detail.response = {}
  }
}
