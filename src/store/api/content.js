import axios from 'axios'

const API_PREFIX = '/api'

export const apiContentList = () => {
  return axios.get(`${API_PREFIX}/fund/list`)
}
