import Vue from 'vue'
import Vuex from 'vuex'

import content from './modules/content'

Vue.use(Vuex)

const createStore = () => {
  return new Vuex.Store({
    modules: {
      content
    }
  })
}

export default createStore
