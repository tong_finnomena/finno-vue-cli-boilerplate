import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../pages/Index.vue'
import Page404 from '../pages/404.vue'
import RouterContent from './content'

Vue.use(VueRouter)

const Router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
      props: true
    },
    RouterContent,
    {
      path: '/404',
      name: '404',
      component: Page404,
      props: true
    },
    { path: '*', component: Page404 }
  ]
})

export default Router
