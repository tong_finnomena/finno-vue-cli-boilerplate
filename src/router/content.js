import Content from '../pages/Content/Index.vue'
import ContentList from '../pages/Content/List.vue'
import ContentDetail from '../pages/Content/Detail.vue'

const routerContent = {
  path: '/content',
  component: Content,
  props: true,
  children: [
    {
      path: '/',
      name: 'content-list',
      component: ContentList,
      props: true
    },
    {
      path: ':id',
      name: 'content-detail',
      component: ContentDetail,
      props: true
    }
  ]
}

export default routerContent
